const mongoose = require('mongoose');
const { getIdFromObj } = require('./utils');

const mapDoc = doc => ({
  _id: doc._id,
  employeeId: getIdFromObj(doc.employee),
  employee: doc.employee,
  shopId: doc.shop,
  amount: doc.amount,
  tipAmount: doc.tipAmount,
  notes: doc.notes,
  date: doc.date,
  createdDate: doc.createdDate,
  modifiedDate: doc.modifiedDate,
  createdBy: doc.createdBy,
  modifiedBy: doc.modifiedBy,
});

const parseJson = jsonObj => ({
  _id: jsonObj._id ? jsonObj._id : mongoose.Types.ObjectId(),
  name: jsonObj.name,
  employee: jsonObj.employeeId,
  amount: jsonObj.amount,
  tipAmount: jsonObj.tipAmount,
  notes: jsonObj.notes,
  shop: jsonObj.shopId,
  date: jsonObj.date,
  createdDate: jsonObj.createdDate,
  modifiedDate: jsonObj.modifiedDate,
  createdBy: jsonObj.createdBy,
  modifiedBy: jsonObj.modifiedBy,
});

module.exports = {
  map: mapDoc,
  parse: parseJson,
};
