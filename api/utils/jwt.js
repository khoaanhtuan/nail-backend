// Generate Token using secret from process.env.JWT_SECRET
const jwt = require('jsonwebtoken');

function generateToken(user) {
  // 1. Dont use password and other sensitive fields
  // 2. Use fields that are useful in other parts of the
  // app/collections/models
  const u = {
    _id: user._id,
    email: user.email,
    name: user.name,
  };
  const token = jwt.sign(u, process.env.JWT_SECRET, {
    expiresIn: 60 * 60 * 24, // expires in 24 hours
  });
  return token;
}


module.exports = {
  generateToken,
};
