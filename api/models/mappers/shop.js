// const _ = require('lodash');
const mongoose = require('mongoose');
const { getIdFromObj, createBaseField } = require('./utils');

const mapDoc = doc => ({
  _id: doc._id,
  name: doc.name,
  ownerId: getIdFromObj(doc.owner),
  owner: doc.owner,
  notes: doc.notes,
  // info: createRequestObj(routesName, doc._id),
});

const parseJson = jsonObj => ({
  ...createBaseField(jsonObj),
  _id: mongoose.Types.ObjectId(),
  name: jsonObj.name,
  owner: jsonObj.ownerId,
  notes: jsonObj.notes,
});

module.exports = {
  parse: parseJson,
  map: mapDoc,
};
