const mongoose = require('mongoose');
const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const { handleError, handleNotFound, createRequestObj, createBaseField, modifiedBaseField } = require('./base');

const Template = require('../models/template');
const User = require('../models/user');
const TemplateType = require('../models/templateType');

const userField = require('../models/fields/user');
const templateField = require('../models/fields/template');
const templateTypeField = require('../models/fields/templateType');

const routeName = '/templates';

exports.templates_get_all = async (req, res) => {
  try {
    const docs = await Template.find()
      .select(templateField)
      .populate('owner', userField)
      .populate('templateType', templateTypeField)
      // .limit(10)
      .exec();
    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => ({
        _id: doc._id,
        name: doc.name,
        ownerRatio: doc.ownerRatio,
        taxRatio: doc.taxRatio,
        ownerId: _.get(doc.owner, '_id', 0),
        owner: doc.owner,
        templateTypeId: _.get(doc.templateType, '_id', 0),
        templateType: doc.templateType,
        descriptions: doc.descriptions,
        notes: doc.notes,
        info: createRequestObj(routeName, doc._id),
      })),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    handleError(err, res);
  }
};


exports.templates_get_by_id = async (req, res) => {
  try {
    const doc = await Template
      .findById(req.params.templateId)
      .select(templateField)
      .populate('owner', userField)
      .populate('templateType', templateTypeField)
      .exec();

    res.status(HttpStatus.OK).json({
      template: {
        name: doc.name,
        ownerRatio: doc.ownerRatio,
        taxRatio: doc.taxRatio,
        ownerId: _.get(doc.owner, '_id', 0),
        owner: doc.owner,
        templateTypeId: _.get(doc.templateType, '_id', 0),
        templateType: doc.templateType,
        descriptions: doc.descriptions,
        notes: doc.notes,
      },
      info: createRequestObj(routeName, doc._id),
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.templates_post = async (req, res) => {
  try {
    const user = await User.findById(req.body.ownerId).exec();
    if (!user) {
      return handleNotFound(res, 'Owner not found');
    }
    const templateType = await TemplateType.findById(req.body.templateTypeId).exec();

    if (!templateType) {
      return handleNotFound(res, 'Template type not found');
    }
    const template = new Template({
      ...createBaseField(res),
      _id: mongoose.Types.ObjectId(),
      name: req.body.name,
      ownerRatio: req.body.ownerRatio,
      taxRatio: req.body.taxRatio,
      owner: req.body.ownerId,
      templateType: req.body.templateTypeId,
      descriptions: req.body.descriptions,
      notes: req.body.notes,
    });

    const result = await template.save();
    res.status(HttpStatus.CREATED).json({
      message: 'Template created',
      data: {
        _id: result._id,
        name: result.name,
        ownerRatio: result.ownerRatio,
        taxRatio: result.taxRatio,
        ownerId: _.get(result.owner, '_id'),
        owner: result.owner,
        templateTypeId: _.get(result.templateType, '_id'),
        templateType: result.templateType,
        descriptions: result.descriptions,
        notes: result.notes,
        info: createRequestObj(routeName, result._id),
      },
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.templates_update = async (req, res) => {
  try {
    const user = await User.findById(req.body.ownerId).exec();
    if (!user) {
      return handleNotFound(res, 'Owner not found');
    }
    const templateType = await TemplateType.findById(req.body.templateTypeId).exec();

    if (!templateType) {
      return handleNotFound(res, 'Template type not found');
    }

    const id = req.params.templateId;
    const template = await Template.findById(id);
    if (template) {
      const updatedOps = {
        ...modifiedBaseField(res),
      };
      _.each(_.omit(req.body, ['_id']), (value, key) => {
        _.set(updatedOps, key, value);
      });
      const result = await Template.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();

      res.status(HttpStatus.OK).json({
        message: 'Template updated',
        data: result,
      });
    } else {
      handleNotFound(res);
    }
  } catch (err) {
    handleError(err, res);
  }
};

exports.templates_delete_by_id = async (req, res) => {
  try {
    await Template.remove({
      _id: req.params.templateId,
    });
    res.status(HttpStatus.OK).json({
      message: 'Template deleted',
    });
  } catch (err) {
    handleError(err, res);
  }
};
