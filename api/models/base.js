const mongoose = require('mongoose');

const { Schema } = mongoose;

module.exports = {
  createdBy: {
    type: Schema.Types.ObjectId,
    // required: true,
    ref: 'User',
  },
  modifiedBy: {
    type: Schema.Types.ObjectId,
    // required: true,
    ref: 'User',
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
};
