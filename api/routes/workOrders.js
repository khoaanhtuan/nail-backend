
const express = require('express');
const WorkOrdersController = require('../controllers/workOrders');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();


router.post('/', checkAuth, WorkOrdersController.workOrders_post);

router.patch('/:workOrderId', checkAuth, WorkOrdersController.workOrders_update);

router.delete('/:workOrderId', checkAuth, WorkOrdersController.workOrders_delete_by_id);

router.get('/date', checkAuth, WorkOrdersController.workOrders_get_by_date);

router.get('/:workOrderId', checkAuth, WorkOrdersController.workOrders_get_by_id);

router.get('/', checkAuth, WorkOrdersController.workOrders_get_all);

module.exports = router;
