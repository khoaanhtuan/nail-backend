
const express = require('express');
const UsersController = require('../controllers/users');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/me', checkAuth, UsersController.users_me);

router.get('/', checkAuth, UsersController.users_get_all);

router.get('/:userId', checkAuth, UsersController.users_get_by_id);

router.post('/login', UsersController.users_login);

router.post('/signup', UsersController.users_signup);

router.delete('/:userId', checkAuth, UsersController.users_delete_by_id);

router.post('/refresh', checkAuth, UsersController.users_refresh);

module.exports = router;
