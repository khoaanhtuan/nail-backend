const _ = require('lodash');
const { API_URL } = require('../../constants');

module.exports = {
  getIdFromObj: obj => _.get(obj, '_id', undefined),
  createRequestObj: (url, id, type = 'GET') => ({
    type,
    url: `${API_URL}${url}/${id}`,
  }),
  createBaseField: (res) => {
    const userId = _.get(res, 'user');
    // console.log(userId);
    return {
      createdBy: userId,
      modifiedBy: userId,
    };
  },
  modifiedBaseField: (res) => {
    const userId = _.get(res, 'user');
    // console.log(userId);
    return {
      modifiedBy: userId,
      modifiedDate: new Date().toJSON(),
    };
  },
};