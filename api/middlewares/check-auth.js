const jwt = require('jsonwebtoken');
const HttpStatus = require('http-status-codes');

module.exports = async (req, res, next) => {
  const { authorization } = req.headers;
  try {
    const verifiedToken = await jwt.verify(authorization.split(' ')[1], process.env.JWT_SECRET);
    res.set('user', verifiedToken._id);
    next();
  } catch (err) {
    console.warn(err);
    return res.status(HttpStatus.UNAUTHORIZED).json({
      message: 'Auth failed',
    });
  }
};
