const mongoose = require('mongoose');
const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const {
  handleError, handleNotFound,
  createBaseField, modifiedBaseField,
} = require('./base');

// const { ROUTES } = require('../constants');
const Employee = require('../models/employee');
const Shop = require('../models/shop');
const User = require('../models/user');

const Contract = require('../models/contract');
const employeeField = require('../models/fields/employee');
const contractField = require('../models/fields/contract');

const shopField = require('../models/fields/shop');
const userField = require('../models/fields/user');
const templateField = require('../models/fields/template');

const EmployeeMapper = require('../models/mappers/employee');
// const ContractMapper = require('../models/mappers/contract');
const ContractController = require('./contracts');

// const routesName = ROUTES.EMPLOYEES;

const { ObjectId } = mongoose.Types;

const findEmployeeById = async (id) => {
  const doc = await Employee
    .findById(id)
    .select(employeeField)
    .populate('owner', userField)
    .populate('shop', shopField)
    // .populate('contract', contractField)
    .exec();

  const contracts = await Contract.find({
    employee: new ObjectId(doc._id),
    isDeleted: false,
  }).populate('template', templateField)
    .exec();

  return EmployeeMapper.map(doc, contracts);
};

// TODO: implement paging
exports.employees_get_all = async (req, res) => {
  try {
    const { shopId, name } = req.query;
    // console.log(shopId);
    const queryObj = {};
    if (shopId) {
      _.set(queryObj, 'shop', shopId);
    }
    if (name) {
      _.set(queryObj, 'name', {
        $regex: `.*${name}.*`,
      });
    }
    const docs = await Employee.find(queryObj)
      .select(employeeField)
      .populate('owner', userField)
      .populate('shop', shopField)
      // .populate('contract', contractField)
    // .limit(10)
      .exec();

    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => EmployeeMapper.map(doc)),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};


exports.employees_get_by_id = async (req, res) => {
  try {
    const employee = await findEmployeeById(req.params.employeeId);
    res.status(HttpStatus.OK).json({
      employee,
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.employees_post = async (req, res) => {
  try {
    const owner = await User.findById(req.body.ownerId).exec();
    if (!owner) {
      return handleNotFound(res, 'User not found');
    }

    const shop = await Shop.findById(req.body.shopId).exec();
    if (!shop) {
      return handleNotFound(res, 'Shop not found');
    }
    if (req.body.contractId) {
      const contract = await Contract.findById(req.body.contractId).exec();
      if (!contract) {
        return handleNotFound(res, 'Contract not found');
      }
    }

    const employee = new Employee(EmployeeMapper.parse({
      ...createBaseField(res),
      ...req.body,
    }));

    const result = await employee.save();
    const _id = _.get(result, '_id');

    // handle add contract
    const { contracts } = req.body;
    if (contracts) {
      result.contracts = await ContractController.updateMultipleContracts(contracts, _id, res);
    }
    res.status(HttpStatus.CREATED).json({
      message: 'Employee created',
      data: EmployeeMapper.map(result),
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.employees_update = async (req, res) => {
  try {
    const id = req.params.employeeId;
    const employee = await Employee.findById(id);

    if (employee) {
      const updatedOps = {
        ...modifiedBaseField(res),
      };
      _.each(_.omit(EmployeeMapper.parse(req.body), ['_id']), (value, key) => {
        _.set(updatedOps, key, value);
      });

      await Employee.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();


      const { contracts } = req.body;
      if (contracts) {
        // console.log(contracts);
        await ContractController.updateMultipleContracts(contracts, id, res);
      }

      const updateEmp = await findEmployeeById(id);

      res.status(HttpStatus.OK).json({
        message: 'Employee updated',
        data: updateEmp,
      });
    } else {
      return handleNotFound(res);
    }
  } catch (err) {
    return handleError(err, res);
  }
};

exports.employees_delete_by_id = async (req, res) => {
  try {
    await Employee.remove({
      _id: req.params.employeeId,
    });
    res.status(HttpStatus.OK).json({
      message: 'Employee deleted',
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.employees_get_by_shop_id = async (req, res) => {
  try {
    const { shopId } = req.params;
    const docs = await Employee.find({
      shop: shopId,
    })
      .select(employeeField)
      .populate('owner', userField)
      .populate('shop', shopField)
      .populate('contract', contractField)
      // .limit(10)
      .exec();
    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => (EmployeeMapper.map(doc))),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};
