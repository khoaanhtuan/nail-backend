const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;
const employeeSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  dob: {
    type: Date,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  notes: {
    type: String,
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  contract: {
    type: Schema.Types.ObjectId,
    ref: 'Contract',
  },
  shop: {
    type: Schema.Types.ObjectId,
    ref: 'Shop',
  },
});

module.exports = mongoose.model(
  'Employee',
  employeeSchema,
);
