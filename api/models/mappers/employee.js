const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require('moment');
const { getIdFromObj } = require('./utils');
const ContractMapper = require('./contract');
const AppConst = require('../../constants');

const mapDoc = (doc, contracts) => ({
  _id: doc._id,
  name: doc.name,
  phone: doc.phone,
  dob: doc.dob,
  isActive: doc.isActive,
  ownerId: getIdFromObj(doc.owner),
  owner: doc.owner,
  shopId: getIdFromObj(doc.shop),
  shop: doc.shop,
  contractId: getIdFromObj(doc.contract),
  contracts: _.map(contracts, contract => ContractMapper.map(contract)),
  notes: doc.notes,
  // info: createRequestObj(routesName, doc._id),
});
const parseJson = jsonObj => ({
  _id: mongoose.Types.ObjectId(),
  name: jsonObj.name,
  phone: jsonObj.phone,
  dob:
    moment(jsonObj.dob, AppConst.FORMAT.DATE).isValid()
      ? moment(jsonObj.dob, AppConst.FORMAT.DATE) : undefined,
  isActive: jsonObj.isActive,
  owner: jsonObj.ownerId || undefined,
  contract: jsonObj.contractId > 0 ? jsonObj.contractId : undefined,
  shop: jsonObj.shopId,
  notes: jsonObj.notes,
});

module.exports = {
  map: mapDoc,
  parse: parseJson,
};
