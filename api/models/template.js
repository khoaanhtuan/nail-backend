const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;
const templateSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },

  ownerRatio: {
    type: Number,
    default: 0.5,
  },
  taxRatio: {
    type: Number,
    default: 0.5,
  },
  owner: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  templateType: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'TemplateType',
  },
  descriptions: {
    type: String,
  },
  notes: {
    type: String,
  },

});

module.exports = mongoose.model(
  'Template',
  templateSchema,
);
