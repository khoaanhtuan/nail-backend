
const express = require('express');
const ContractController = require('../controllers/contracts');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/', checkAuth, ContractController.contracts_get_all);

router.get('/:contractId', checkAuth, ContractController.contracts_get_by_id);

router.post('/', ContractController.contracts_post);

router.patch('/:contractId', checkAuth, ContractController.contracts_update);

router.delete('/:contractId', checkAuth, ContractController.contracts_delete_by_id);

module.exports = router;
