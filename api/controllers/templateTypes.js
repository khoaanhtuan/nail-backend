const mongoose = require('mongoose');
const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const {
  handleError, handleNotFound, createRequestObj, createBaseField, modifiedBaseField,
} = require('./base');

const TemplateType = require('../models/templateType');
const templateTypeField = require('../models/fields/templateType');

const routeName = '/templatetype';

exports.templateTypes_get_all = async (req, res) => {
  try {
    const docs = await TemplateType.find()
      .select(templateTypeField)
      .populate('templateType', templateTypeField)
      // .limit(10)
      .exec();
    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => ({
        _id: doc._id,
        name: doc.name,
        descriptions: doc.descriptions,
        notes: doc.notes,
        info: createRequestObj(routeName, doc._id),
      })),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};

exports.templateTypes_get_by_id = async (req, res) => {
  try {
    const templateType = await TemplateType
      .findById(req.params.templateTypeId)
      .select(templateTypeField)
      .exec();

    res.status(HttpStatus.OK).json({
      templateType,
      info: createRequestObj(routeName, templateType._id),
    });
  } catch (err) {
    return handleError(err, res);
  }
};


exports.templateTypes_post = async (req, res) => {
  try {
    const templateType = new TemplateType({
      ...createBaseField(res),
      _id: mongoose.Types.ObjectId(),
      name: req.body.name,
      descriptions: req.body.descriptions,
      notes: req.body.notes,
    });

    const result = await templateType.save();
    res.status(HttpStatus.CREATED).json({
      message: 'Template Type created',
      data: {
        _id: result._id,
        name: result.name,
        descriptions: result.descriptions,
        notes: result.notes,
        info: createRequestObj(routeName, result._id),
      },
    });
  } catch (err) {
    return handleError(err, res);
  }
};


exports.templateTypes_update = async (req, res) => {
  try {
    const id = req.params.templateTypeId;
    const templateType = await TemplateType.findById(id);
    if (templateType) {
      const updatedOps = {
        ...modifiedBaseField(res),
      };
      _.each(_.omit(req.body, ['_id']), (value, key) => {
        _.set(updatedOps, key, value);
      });
      const result = await TemplateType.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();

      return res.status(HttpStatus.OK).json({
        message: 'Template Type updated',
        data: result,
      });
    }
    return handleNotFound(res);
  } catch (err) {
    return handleError(err, res);
  }
};


exports.templateTypes_delete_by_id = async (req, res) => {
  try {
    await TemplateType.remove({
      _id: req.params.templateTypeId,
    });
    return res.status(HttpStatus.OK).json({
      message: 'Template type deleted',
    });
  } catch (err) {
    handleError(err, res);
  }
};
