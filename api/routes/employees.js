
const express = require('express');
const EmployeesController = require('../controllers/employees');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/', checkAuth, EmployeesController.employees_get_all);

// router.get('/shop/:shopId', checkAuth, EmployeesController.employees_get_by_shop_id);

router.get('/:employeeId', checkAuth, EmployeesController.employees_get_by_id);

router.post('/', checkAuth, EmployeesController.employees_post);

router.patch('/:employeeId', checkAuth, EmployeesController.employees_update);

router.delete('/:employeeId', checkAuth, EmployeesController.employees_delete_by_id);

module.exports = router;
