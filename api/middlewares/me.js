const jwt = require('jsonwebtoken');
const HttpStatus = require('http-status-codes');

module.exports = async (req, res) => {
  const { authorization } = req.headers;
  try {
    const verifiedToken = await jwt.verify(authorization.split(' ')[1], process.env.JWT_SECRET);
    return res.status(HttpStatus.OK).json(verifiedToken);
  } catch (err) {
    console.warn(err);
    return res.status(HttpStatus.UNAUTHORIZED).json({
      message: 'Auth failed',
    });
  }
};
