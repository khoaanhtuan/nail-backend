// const _ = require('lodash');
const mongoose = require('mongoose');
const _ = require('lodash');
const {
  getIdFromObj,
  // createBaseField,
} = require('./utils');

const mapDoc = doc => ({
  _id: doc._id,
  name: doc.name,
  templateId: getIdFromObj(doc.template),
  template: doc.template,
  employeeId: getIdFromObj(doc.employee),
  employee: _.get(doc, 'employee'),
  shopId: getIdFromObj(doc.shop),
  shop: doc.shop,
  validDate: doc.validDate,
  isExpired: doc.isExpired,
  notes: doc.notes,
  ownerRatio: doc.ownerRatio,
  taxRatio: doc.taxRatio,
  // info: createRequestObj(routesName, doc._id),
});

const parseJson = jsonObj => ({
  // ...createBaseField(jsonObj),
  _id: _.get(jsonObj, '_id', mongoose.Types.ObjectId()),
  name: jsonObj.name,
  employee: jsonObj.employeeId,
  template: jsonObj.templateId,
  shop: _.get(jsonObj, 'shopId', mongoose.Types.ObjectId()),
  validDate: jsonObj.validDate,
  isExpired: jsonObj.isExpired,
  ownerRatio: jsonObj.ownerRatio,
  taxRatio: jsonObj.taxRatio,
  notes: jsonObj.notes,
});

module.exports = {
  parse: parseJson,
  map: mapDoc,
};
