const express = require('express');
const multer = require('multer');

const checkAuth = require('../middlewares/check-auth');
const ProductsController = require('../controllers/products');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, `${new Date().toISOString()}_${file.originalname}`);
  },
});

const router = express.Router();

const upload = multer({
  storage,
  limits: {
    fieldSize: 1024 * 1024 * 5,
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      // Reject a file
      cb(null, false);
    }
  },
});

router.get('/', ProductsController.products_get_all);

router.post('/', checkAuth, upload.single('productImage'), ProductsController.products_add_item);

router.get('/:productId', ProductsController.products_get_by_id);

router.patch('/:productId', checkAuth, ProductsController.products_update_item);

router.delete('/:productId', checkAuth, ProductsController.products_delete_by_id);

module.exports = router;
