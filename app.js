const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const { ROUTES } = require('./api/constants');

mongoose.connect(`mongodb+srv://${process.env.MONGO_ATLAS_USER}:${process.env.MONGO_ATLAS_PWD}@cluster0-sigul.mongodb.net/test?retryWrites=true`,
  {
    useNewUrlParser: true,
  });

const app = express();
// const productRoutes = require('./api/routes/products');
// const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/users');
const templateTypeRoutes = require('./api/routes/templatetypes');
const shopRoutes = require('./api/routes/shops');
const templateRoutes = require('./api/routes/templates');
const contractRoutes = require('./api/routes/contracts');
const employeeRoutes = require('./api/routes/employees');
const workOrderRoutes = require('./api/routes/workOrders');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/uploads', express.static('uploads'));
// CORS support
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Oyrigin', '*');
  res.header('Access-Control-Allow-Headers', '*');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json();
  }
  next();
});

// Request routes
// app.use(ROUTES.PRODUCTS, productRoutes);
// app.use('/orders', orderRoutes);
app.use(ROUTES.USERS, userRoutes);
app.use(ROUTES.TEMPLATE_TYPES, templateTypeRoutes);
app.use(ROUTES.SHOPS, shopRoutes);
app.use(ROUTES.TEMPLATES, templateRoutes);
app.use(ROUTES.CONTRACTS, contractRoutes);
app.use(ROUTES.EMPLOYEES, employeeRoutes);
app.use(ROUTES.WORK_ORDERS, workOrderRoutes);
//
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
