const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;

const contractSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    require: true,
  },
  template: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Template',
  },
  employee: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Employee',
  },
  ownerRatio: {
    type: Number,
    default: 0.5,
  },
  taxRatio: {
    type: Number,
    default: 0.5,
  },
  shop: {
    type: Schema.Types.ObjectId,
    // required: true,
    ref: 'Shop',
  },
  validDate: {
    type: Date,
    required: true,
  },
  isExpired: {
    type: Boolean,
    default: false,
  },
  notes: {
    type: String,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model(
  'Contract',
  contractSchema,
);
