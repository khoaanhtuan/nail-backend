const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const {
  handleError, handleNotFound, createBaseField, modifiedBaseField,
} = require('./base');

const ContractMapper = require('../models/mappers/contract');

const Contract = require('../models/contract');
const Employee = require('../models/employee');
const Shop = require('../models/shop');
const Template = require('../models/template');

const employeeField = require('../models/fields/employee');
const shopField = require('../models/fields/shop');
const contractField = require('../models/fields/contract');
const templateField = require('../models/fields/template');

// const routesName = '/contracts';

const findContractById = async (id) => {
  const contract = await Contract
    .findById(id)
    .select(contractField)
    .populate('template', templateField)
    .populate('employee', employeeField)
    .populate('shop', shopField)
    .exec();
  return ContractMapper.map(contract);
};


const markContractDeleted = async (id) => {
  await Contract.update({
    _id: id,
  }, {
    $set: {
      isDeleted: true,
    },
  }).exec();
  // console.log(ops);
  return id;
};


const updateContract = async (contract, res) => {
  const updatedOps = {
    ...modifiedBaseField(res),
  };
  _.each(_.omit(contract, '_id'), (value, key) => {
    _.set(updatedOps, key, value);
  });
  const id = contract._id;
  await Contract.update({
    _id: id,
  }, {
    $set: updatedOps,
  }).exec();
  const updatedContract = await findContractById(id);
  return ContractMapper.map(updatedContract);
};

// const updateContracts = async (contracts) => {
//   const updatedOps = _.map(contracts, contract => updateContract(contract));
//   const result = await Promise.all(updatedOps);
//   return result;
// };

exports.contracts_get_all = async (req, res) => {
  try {
    const { shopId, employeeId } = req.query;

    const queryObj = {
      isDeleted: false,
    };
    if (employeeId) {
      _.set(queryObj, 'employee', employeeId);
    }
    if (shopId) {
      _.set(queryObj, 'shop', shopId);
    }
    const docs = await Contract.find(queryObj)
      // .select(contractField)
      .populate('template', templateField)
      .populate('employee', employeeField)
      .populate('shop', shopField)
      // .limit(10)
      .exec();

    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => ContractMapper.map(doc)),
    };
    return res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};

exports.contracts_get_by_id = async (req, res) => {
  try {
    const contract = findContractById(req.params.contractId);

    res.status(HttpStatus.OK).json({
      contract,
      // info: createRequestObj(routesName, contract._id),
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.contracts_post = async (req, res) => {
  try {
    const employee = await Employee.findById(req.body.employeeId).exec();
    if (!employee) {
      return handleNotFound(res, 'Employee not found');
    }
    if (req.body.shopId > 0) {
      const shop = await Shop.findById(req.body.shopId).exec();
      if (!shop) {
        return handleNotFound(res, 'Shop not found');
      }
    }
    const template = await Template.findById(req.body.templateId).exec();
    if (!template) {
      return handleNotFound(res, 'Template not found');
    }
    const { body } = req;
    const contract = new Contract(ContractMapper.parse({
      ...createBaseField(res),
      ...body,
    }));

    const result = await contract.save();
    return res.status(HttpStatus.CREATED).json({
      message: 'Contract created',
      data: ContractMapper.map(result),
    });
  } catch (err) {
    return handleError(err, res);
  }
};


exports.contracts_update = async (req, res) => {
  try {
    const id = req.params.contractId;
    const contract = await Contract.findById(id);
    if (contract) {
      const updatedContract = await updateContract(contract, res);
      res.status(HttpStatus.OK).json({
        message: 'Contract updated',
        data: updatedContract,
      });
    } else {
      return handleNotFound(res);
    }
  } catch (err) {
    handleError(err, res);
  }
};

exports.contracts_delete_by_id = async (req, res) => {
  try {
    await Contract.remove({
      _id: req.params.contractId,
    });
    return res.status(HttpStatus.OK).json({
      message: 'Contract deleted',
    });
  } catch (err) {
    return handleError(err, res);
  }
};


exports.updateMultipleContracts = async (contracts, employeeId, res) => {
  // Handle update
  // console.log(contracts);

  const updateList = _.filter(contracts, contract => contract._id);
  // console.log('Update:', updateList);
  const dbContracts = await Contract.find({
    employee: employeeId,
    isDeleted: false,
  }).exec();
  // console.log(dbContracts);
  const ops = _.map(dbContracts, (dbContract) => {
    console.log(`checking ${dbContract._id}`);
    const found = _.find(updateList, item => _.eq(`${item._id}`, `${dbContract._id}`));
    if (found) {
      console.log('Updating ', found._id);
      return updateContract(found, res);
    }
    console.log(`Deleting ${dbContract._id}`);
    // _.set(dbContract, 'isDeleted', true);
    return markContractDeleted(dbContract._id);
  });
  const updated = await Promise.all(ops);
  // console.log(updated);
  const insertList = _.filter(contracts, contract => !contract._id);
  const contractDocs = _.map(insertList, contractJson => new Contract(
    ContractMapper.parse({
      ...createBaseField(res),
      ...contractJson,
      employeeId,
    }),
  ));
  // Handle insert
  const inserted = await Contract.insertMany(contractDocs);

  return [inserted, updated];
};
