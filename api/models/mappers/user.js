// const _ = require('lodash');
const mongoose = require('mongoose');
const _ = require('lodash');
const { createBaseField } = require('./utils');

const mapDoc = doc => ({
  _id: doc._id,
  name: doc.name,
  email: doc.email,
  createdDate: doc.createdDate,
  modifiedDate: doc.modifiedDate,
  createdBy: doc.createdBy,
  modifiedBy: doc.modifiedBy,
});

const parseJson = jsonObj => ({
  ...createBaseField(jsonObj),
  _id: _.get(jsonObj, '_id', mongoose.Types.ObjectId()),
  name: jsonObj.name,
  email: jsonObj.email,
  createdBy: jsonObj.createdBy,
  modifiedBy: jsonObj.modifiedBy,
  createdDate: jsonObj.createdDate,
  modifiedDate: jsonObj.modifiedDate,
});

module.exports = {
  parse: parseJson,
  map: mapDoc,
};
