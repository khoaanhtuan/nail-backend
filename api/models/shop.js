const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;
const shopSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  owner: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  notes: {
    type: String,
  },
});

module.exports = mongoose.model(
  'Shop',
  shopSchema,
);
