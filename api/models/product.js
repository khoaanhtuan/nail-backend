
const mongoose = require('mongoose');

const { Schema } = mongoose;
const productSchema = mongoose.Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
    min: [0, 'Price cannot less than zero'],
  },
  productImage: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model(
  'Product',
  productSchema,
);
