const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const { API_URL } = require('../constants');

module.exports = {
  handleError: (err, res) => {
    console.warn(err);
    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  },
  handleNotFound: (res, message = 'NOT_FOUND') => res.status(HttpStatus.NOT_FOUND).json({ message }),
  createRequestObj: (url, id, type = 'GET') => ({
    type,
    url: `${API_URL}${url}/${id}`,
  }),
  getIdFromObj: obj => _.get(obj, '_id', 0),
  createBaseField: (res) => {
    const userId = res.get('user');
    return {
      createdBy: userId,
      modifiedBy: userId,
      createdDate: new Date().toJSON(),
      modifiedDate: new Date().toJSON(),
    };
  },
  modifiedBaseField: (res) => {
    const userId = res.get('user');
    return {
      modifiedBy: userId,
      modifiedDate: new Date().toJSON(),
    };
  },
};
