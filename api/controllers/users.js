const mongoose = require('mongoose');
const HttpStatus = require('http-status-codes');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const { handleError } = require('./base');
const me = require('../middlewares/me');
const JwtHepler = require('../utils/jwt');

const UserMapper = require('../models/mappers/user');

// const routeName = '/users';

exports.users_get_all = async (req, res) => {
  try {
    const docs = await User.find()
      .select('_id name email')
      .limit(10).exec();
    const response = {
      count: docs.length,
      data: _.map(docs, doc => UserMapper.map(doc)),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    handleError(err, res);
  }
};

exports.users_get_by_id = async (req, res) => {
  try {
    const doc = await User.findById(req.params.userId)
      .select('_id name email')
      .exec();
    const response = {
      user: UserMapper.map(doc),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    handleError(err, res);
  }
};

exports.users_login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const users = await User.find({
      email,
    }).exec();
    if (!_.isEmpty(users)) {
      const user = _.first(users);
      const isPwdMatch = await bcrypt.compare(password, user.password);
      if (isPwdMatch) {
        const jwtToken = JwtHepler.generateToken(user);

        return res.status(HttpStatus.OK).json({
          message: 'Auth success',
          token: jwtToken,
          user: UserMapper.map(user),
        });
      }
    }
    return res.status(HttpStatus.UNAUTHORIZED).json({
      message: 'Invalid login',
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.users_signup = async (req, res) => {
  try {
    const { name, email, password } = req.body;
    try {
      const existingUser = await User.find({
        email,
      }).exec();
      if (!_.isEmpty(existingUser)) {
        // console.log(existingUser);
        return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
          message: 'Email existed',
        });
      }
    } catch (err) {
      console.warn(err);
    }

    const saltRounds = await bcrypt.genSalt(10);
    const hashedPwd = await bcrypt.hash(password, saltRounds);
    const user = new User({
      _id: new mongoose.Types.ObjectId(),
      name,
      email,
      password: hashedPwd,
    });
    const result = await user.save();
    // console.log(result);
    res.status(HttpStatus.CREATED).json({
      message: 'User created',
      data: result,
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.users_delete_by_id = async (req, res) => {
  // const user = await User.findById(req.params.userId);
  try {
    await User.remove({ _id: req.params.userId });
    res.status(HttpStatus.OK).json({
      message: 'User deleted',
    });
  } catch (err) {
    handleError(err, res);
  }
};

exports.users_me = async (req, res) => me(req, res);

exports.users_refresh = async (req, res) => {
  const { token } = req.body;
  try {
    const verifiedToken = await jwt.verify(token, process.env.JWT_SECRET);

    const jwtToken = JwtHepler.generateToken(verifiedToken);

    return res.status(HttpStatus.OK).json({
      message: 'success',
      token: jwtToken,
    });
  } catch (err) {
    // console.warn(err);
    return res.status(HttpStatus.UNAUTHORIZED).json({
      message: 'Auth failed',
    });
  }
};
