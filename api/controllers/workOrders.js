const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const moment = require('moment');
const {
  handleError, handleNotFound,
  createBaseField, modifiedBaseField,
} = require('./base');
const APP_CONST = require('../constants');
const WorkOrder = require('../models/workOrder');
const Employee = require('../models/employee');

const employeeField = require('../models/fields/employee');
const workOrderField = require('../models/fields/workOrder');
const WorkOrderMapper = require('../models/mappers/workOrder');

// const routesName = '/workorders';

const findById = async (id) => {
  const doc = await WorkOrder
    .findById(id)
    .select(workOrderField)
    .populate('employee', employeeField)
    .exec();

  const workOrder = WorkOrderMapper.map(doc);
  return workOrder;
};

exports.workOrders_get_all = async (req, res) => {
  try {
    const docs = await WorkOrder.find()
      .select(workOrderField)
      .populate('employee', employeeField)
      .limit(10)
      .exec();

    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => (WorkOrderMapper.map(doc))),
    };
    return res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};

exports.workOrders_get_by_date = async (req, res) => {
  const { start, end, shopIds } = req.query;
  // console.log(start, end);
  const query = {};
  if (start) {
    const parsedStart = moment(start, APP_CONST.FORMAT.DATE);
    _.set(query, 'createdDate.$gte', parsedStart.toDate());
  }
  if (end) {
    const parsedEnd = moment(end, APP_CONST.FORMAT.DATE);
    _.set(query, 'createdDate.$lte', parsedEnd.toDate());
  }
  if (shopIds) {
    const ids = _.split(shopIds, ',');
    _.set(query, 'shop.$in', [...ids]);
  }
  // console.log(query);
  const docs = await WorkOrder
    .find(query)
    .populate('employee', employeeField)
    .exec();
  const response = {
    meta: {
      count: docs.length,
      date: {
        start,
        end,
      },
    },
    data: _.map(docs, doc => (WorkOrderMapper.map(doc))),
  };
  return res.status(HttpStatus.OK).json(response);
};


exports.workOrders_get_by_id = async (req, res) => {
  try {
    const workOrder = await findById(req.params.workOrderId);
    res.status(HttpStatus.OK).json(workOrder);
  } catch (err) {
    handleError(err, res);
  }
};

exports.workOrders_post = async (req, res) => {
  try {
    const employee = await Employee.findById(req.body.employeeId).exec();
    if (!employee) {
      return handleNotFound(res, 'Employee not found');
    }
    const { shop } = employee;
    const { body } = req;
    const workOrder = new WorkOrder({
      ...createBaseField(req),
      ...WorkOrderMapper.parse({ ...body, shopId: shop }),
    });

    const result = await workOrder.save();
    return res.status(HttpStatus.CREATED).json({
      message: 'Work Order created',
      data: WorkOrderMapper.map(result),
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.workOrders_update = async (req, res) => {
  try {
    const id = req.params.workOrderId;
    const shop = await WorkOrder.findById(id);
    if (shop) {
      const baseField = modifiedBaseField(res);
      const updatedOps = {
        ...baseField,
      };
      _.each(req.body, (value, key) => {
        _.set(updatedOps, key, value);
      });
      await WorkOrder.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();

      const updatedWorkOrder = findById(id);
      res.status(HttpStatus.OK).json({
        message: 'Work Order updated',
        data: updatedWorkOrder,
      });
    } else {
      return handleNotFound(res);
    }
  } catch (err) {
    handleError(err, res);
  }
};

exports.workOrders_delete_by_id = async (req, res) => {
  try {
    await WorkOrder.remove({
      _id: req.params.workOrderId,
    });
    return res.status(HttpStatus.OK).json({
      message: 'Work Order deleted',
    });
  } catch (err) {
    return handleError(err, res);
  }
};
