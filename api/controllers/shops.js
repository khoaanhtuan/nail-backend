const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const {
  handleError, handleNotFound, createRequestObj, createBaseField, modifiedBaseField,
} = require('./base');

const Shop = require('../models/shop');
const User = require('../models/user');
const userField = require('../models/fields/user');
const shopField = require('../models/fields/shop');
const ShopMapper = require('../models/mappers/shop');

const routesName = '/shops';

exports.shops_get_all = async (req, res) => {
  try {
    const docs = await Shop.find()
      .select(shopField)
      .populate('owner', userField)
      // .limit(10)
      .exec();
    const response = {
      meta: {
        count: docs.length,
      },
      data: _.map(docs, doc => ShopMapper.map(doc)),
    };
    return res.status(HttpStatus.OK).json(response);
  } catch (err) {
    return handleError(err, res);
  }
};


exports.shops_get_by_id = async (req, res) => {
  try {
    const shop = await Shop
      .findById(req.params.shopId)
      .select(shopField)
      .populate('owner', userField)
      .exec();

    return res.status(HttpStatus.OK).json({
      shop,
      info: createRequestObj(routesName, shop._id),
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.shops_post = async (req, res) => {
  try {
    const user = await User.findById(req.body.ownerId).exec();
    if (!user) {
      return handleNotFound(res);
    }
    const shop = new Shop(ShopMapper.parse({
      ...createBaseField(res),
      ...req.body,
    }));

    const result = await shop.save();
    res.status(HttpStatus.CREATED).json({
      message: 'Shop created',
      data: ShopMapper.map(result),
    });
  } catch (err) {
    return handleError(err, res);
  }
};

exports.shops_update = async (req, res) => {
  try {
    const id = req.params.shopId;
    const shop = await Shop.findById(id);
    if (shop) {
      const updatedOps = {
        ...modifiedBaseField(res),
      };
      _.each(_.omit(req.body, ['_id']), (value, key) => {
        _.set(updatedOps, key, value);
      });
      await Shop.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();

      const updatesShop = await Shop
        .findById(req.params.shopId)
        .select(shopField)
        .populate('owner', userField)
        .exec();

      res.status(HttpStatus.OK).json({
        message: 'Shop updated',
        data: ShopMapper.map(updatesShop),
      });
    } else {
      return handleNotFound(res);
    }
  } catch (err) {
    return handleError(err, res);
  }
};

exports.shops_delete_by_id = async (req, res) => {
  try {
    await Shop.remove({
      _id: req.params.shopId,
    });
    res.status(HttpStatus.OK).json({
      message: 'Shop deleted',
    });
  } catch (err) {
    return handleError(err, res);
  }
};
