const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;
const templateTypeSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  descriptions: {
    type: String,
  },
  notes: {
    type: String,
  },

});

module.exports = mongoose.model(
  'TemplateType',
  templateTypeSchema,
);
