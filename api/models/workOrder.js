const mongoose = require('mongoose');
const BaseModel = require('./base');

const { Schema } = mongoose;
const workOrderSchema = mongoose.Schema({
  ...BaseModel,
  _id: Schema.Types.ObjectId,
  employee: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Employee',
  },
  shop: {
    type: Schema.Types.ObjectId,
    ref: 'Shop',
  },
  amount: {
    type: Number,
    required: true,
  },
  tipAmount: {
    type: Number,
    required: true,
  },
  notes: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model(
  'WorkOrder',
  workOrderSchema,
);
