
const express = require('express');
const ShopsController = require('../controllers/shops');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/', checkAuth, ShopsController.shops_get_all);

router.get('/:shopId', checkAuth, ShopsController.shops_get_by_id);

router.post('/', ShopsController.shops_post);

router.patch('/:shopId', checkAuth, ShopsController.shops_update);

router.delete('/:shopId', checkAuth, ShopsController.shops_delete_by_id);

module.exports = router;
