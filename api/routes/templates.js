
const express = require('express');
const TemplatesController = require('../controllers/templates');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/', checkAuth, TemplatesController.templates_get_all);

router.get('/:templateId', checkAuth, TemplatesController.templates_get_by_id);

router.post('/', checkAuth, TemplatesController.templates_post);

router.patch('/:templateId', checkAuth, TemplatesController.templates_update);

router.delete('/:templateId', checkAuth, TemplatesController.templates_delete_by_id);

module.exports = router;
