const HttpStatus = require('http-status-codes');
const mongoose = require('mongoose');
const _ = require('lodash');

const Order = require('../models/order');
const Product = require('../models/product');

exports.orders_get_all = async (req, res) => {
  try {
    const orders = await Order.find()
      .select('_id name quantity product')
      .populate('product', '_id name price')
      .limit(10)
      .exec();
    // console.log(orders);
    const response = {
      count: orders.length,
      orders: _.map(orders, order => ({
        _id: order._id,
        name: order.name,
        quantity: order.quantity,
        product: order.product,
        request: {
          type: 'GET',
          url: `localhost:3000/orders/${order._id}`,
        },
      })),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.orders_post = async (req, res) => {
  try {
    const product = await Product.findById(req.body.productId).exec();
    if (!product) {
      res.status(404).json({
        message: 'Product not found',
      });
    }
    console.log(product);
    const order = new Order({
      _id: mongoose.Types.ObjectId(),
      name: req.body.name,
      quantity: req.body.quantity,
      product: req.body.productId,
    });

    const result = await order.save();
    res.status(201).json({
      message: 'Order created',
      data: {
        _id: result._id,
        name: result.name,
        quantity: result.quantity,
        product: result.product,
        request: {
          type: 'GET',
          url: `localhost:3000/products/${result._id}`,
        },
      },
    });
  } catch (err) {
    console.warn(err);
    res.status(500).json(err);
  }
};


exports.orders_get_by_id = async (req, res) => {
  try {
    const order = await Order.findById(req.params.orderId).select('_id name quantity product')
      .populate('product', '_id name price');
    res.status(200).json({
      order,
      request: {
        type: 'GET',
        url: `localhost:3000/order/${order._id}`,
      },
    });
  } catch (err) {
    console.warn(err);
    res.status(500).json(err);
  }
};

exports.orders_update = async (req, res) => {
  try {
    const id = req.params.orderId;
    const order = await Order.findById(id);
    if (order) {
      const updatedOps = {};
      _.each(req.body, (value, key) => {
        _.set(updatedOps, key, value);
      });
      const result = await Order.update({
        _id: id,
      }, {
        $set: updatedOps,
      }).exec();

      res.status(HttpStatus.OK).json({
        message: 'Order updated',
        data: result,
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        message: 'Order not found',
      });
    }
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.orders_delete_by_id = async (req, res) => {
  try {
    const result = await Order.remove({
      _id: req.params.orderId,
    });
    res.status(HttpStatus.OK).json({
      message: 'Order deleted',
      data: result,
    });
  } catch (err) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};
