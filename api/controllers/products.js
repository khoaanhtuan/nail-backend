const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const mongoose = require('mongoose');

const Product = require('../models/product');

exports.products_get_all = async (req, res) => {
  try {
    const docs = await Product.find()
      .select('_id name price productImage')
      .limit(10).exec();
    // console.log(docs);
    const response = {
      count: docs.length,
      products: _.map(docs, doc => ({
        _id: doc._id,
        name: doc.name,
        price: doc.price,
        productImage: doc.productImage ? `localhost:3000/${doc.productImage}` : null,
        request: {
          type: 'GET',
          url: `localhost:3000/products/${doc._id}`,
        },
      })),
    };
    res.status(HttpStatus.OK).json(response);
  } catch (err) {
    console.log(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.products_add_item = async (req, res) => {
  try {
    // console.log(req.file);
    const product = new Product({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      price: req.body.price,
      productImage: req.file.path,
    });
    const result = await product.save();
    res.status(HttpStatus.CREATED).json({
      message: 'Product created',
      data: {
        _id: result._id,
        name: result.name,
        price: result.price,
        productImage: result.productImage ? `localhost:3000/${result.productImage}` : null,
        request: {
          type: 'GET',
          url: `localhost:3000/products/${result._id}`,
        },
      },
    });
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.products_get_by_id = async (req, res) => {
  try {
    const id = req.params.productId;
    // console.log(id);
    const doc = await Product.findById(id)
      .select('name price _id productImage')
      .exec();
    if (doc) {
      res.status(HttpStatus.OK).json({
        _id: doc._id,
        name: doc.name,
        price: doc.price,
        productImage: doc.productImage ? `localhost:3000/${doc.productImage}` : null,
        request: {
          type: 'GET',
          url: `localhost:3000/products/${doc._id}`,
        },
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        message: 'No valid entry ',
      });
    }
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.products_update_item = async (req, res) => {
  try {
    const id = req.params.productId;
    const updatedOps = {};
    _.each(req.body, (value, key) => {
      _.set(updatedOps, key, value);
    });

    const result = await Product.update({
      _id: id,
    }, {
      $set: updatedOps,
    }).exec();
    res.status(HttpStatus.OK).json({
      message: 'Product updated',
      data: result,
    });
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};

exports.products_delete_by_id = async (req, res) => {
  try {
    const id = req.params.productId;

    await Product.remove({
      _id: id,
    }).exec();
    res.status(HttpStatus.OK).json({
      message: 'Product deleted',
    });
  } catch (err) {
    console.warn(err);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
  }
};
