
const express = require('express');

const checkAuth = require('../middlewares/check-auth');
const OrdersController = require('../controllers/orders');

const router = express.Router();

router.get('/', checkAuth, OrdersController.orders_get_all);

router.post('/', checkAuth, OrdersController.orders_post);

router.get('/:orderId', OrdersController.orders_get_by_id);

router.patch('/:orderId', checkAuth, OrdersController.orders_update);

router.delete('/:orderId', checkAuth, OrdersController.orders_delete_by_id);

module.exports = router;
