module.exports = {
  API_URL: process.env.API_URL,
  ROUTES: {
    EMPLOYEES: '/employees',
    CONTRACTS: '/contracts',
    TEMPLATES: '/templates',
    SHOPS: '/shops',
    TEMPLATE_TYPES: '/templatetypes',
    USERS: '/users',
    WORK_ORDERS: '/workorders',
    // ORDERS: '/orders',
    // PRODUCTS: '/products',
  },
  FORMAT: {
    DATE: 'DD-MM-YYYY',
  },
};
