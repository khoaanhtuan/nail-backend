
const express = require('express');
const TemplateTypesController = require('../controllers/templateTypes');
const checkAuth = require('../middlewares/check-auth');

const router = express.Router();

router.get('/', checkAuth, TemplateTypesController.templateTypes_get_all);

router.get('/:templateTypeId', checkAuth, TemplateTypesController.templateTypes_get_by_id);

router.post('/', TemplateTypesController.templateTypes_post);

router.patch('/:templateTypeId', checkAuth, TemplateTypesController.templateTypes_update);

router.delete('/:templateTypeId', checkAuth, TemplateTypesController.templateTypes_delete_by_id);

module.exports = router;
